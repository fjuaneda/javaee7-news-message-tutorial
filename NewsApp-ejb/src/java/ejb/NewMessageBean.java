package ejb;

import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.ejb.MessageDrivenContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/NewMessage"),
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue")
})
public class NewMessageBean implements MessageListener {
    @Resource
    private MessageDrivenContext mdc;

    @PersistenceContext(unitName = "NewsApp-ejbPU")
    private EntityManager em;

    @Override
    public void onMessage(final Message message) {
        ObjectMessage msg = null;
        try {
            if(message instanceof ObjectMessage) {
                msg = (ObjectMessage) message;

                final NewsEntity e = (NewsEntity) msg.getObject();
                save(e);
            }
        } catch(final JMSException e) {
            e.printStackTrace();

            mdc.setRollbackOnly();
        } catch(final Throwable te) {
            te.printStackTrace();
        }
    }

    public void save(final Object object) {
        em.persist(object);
    }
}
