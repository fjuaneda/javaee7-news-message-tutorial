package web;

import ejb.NewsEntity;
import ejb.NewsEntityFacade;
import ejb.SessionManagerBean;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ListNews", urlPatterns = {"/ListNews"})
public class ListNews extends HttpServlet {
    @Inject
    private SessionManagerBean sessionManagerBean;
    @Inject
    private NewsEntityFacade newsEntityFacade;

    protected void processRequest(
        final HttpServletRequest request,
        final HttpServletResponse response
    ) throws
        ServletException,
        IOException
    {
        request.getSession(true);
        response.setContentType("text/html;charset=UTF-8");

        try(final PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListNews</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListNews at " + request.getContextPath() + "</h1>");

            final List<NewsEntity> news = newsEntityFacade.findAll();
            news.stream().forEachOrdered(elem -> {
               out.println(" <b>"+ elem.getTitle()+" </b><br />");
               out.println(elem.getBody()+"<br /> ");
            });

            out.println("<a href='PostMessage'>Add new message</a>");

            out.println("<br><br>");
            out.println(sessionManagerBean.getActiveSessionsCount() + " user(s) reading the news.");

            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)
        throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "List available messages from database as posted through JMS";
    }
}
